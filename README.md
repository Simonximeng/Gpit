<!---
Add picture/signs of Gpit at the top in the future
-->
## Premise/Assumption/Axiom


### p

0. **This theory is perceptually simple and easy** 
1. Animals need **perception** to detect and thus interact with info; info detection is perception 
1. The simplest form of perceivable info is **binary**. e.g. 1/0, yes/no
1. The fundamental info is **0/nothing/void/null**
1. Animals **crave/desire** (more) info
1. Animals prefer/favor **easy/simple** info 
1. Perception need some forms of **resource** 
1. Resources are typically/usually **finite/limited** 
1. All resources are essentially **time** resources derivatives

### i

1. Every/Any/All thing/stuff/matter is some forms of **information (info)**; even mind, ideas, feelings 
1. There are **infinitely** many forms of info; from now on, 'forms of' are usually ommitted
1. Info can/may/will be **shrunk/reduced** into simpler form, but this process is **irreversible/permanent**

The 0th premises (pi.0) is **the most important premises** which defines all other premises. The reasons of those premises and the name Gpit, including why P0 is the most important premise, will be revealed once ready (...ror). I believe Gpit can answer many questions...

<!---
- What is the meaning of life? Is life meaningless?
- 人性本善或恶?
- Materialism or Idealism?
- What is the truth of the nature?
- What is happiness and how?
- Riemann hypothesis
- P vs NP
- Navier-Stokes 
--->

Here is one meaning of Gpit:  
	
_**G**eneral **p**erception-of-**i**nformation **t**heory_

----
Proceed if meet any of: 
- [ ] More than 18 years old 
- [ ] Passed puberty  
Why those criterion?...ror

----

Word/Letter/Abbr\*/Symbol | Pronounce | Usage/Meaning | Example or Notes |
----| ---- | ----| ----
| L.T. | Linus Torvalds | | L.T. is the creator of Linux kernel and Git
| B.S. | Bjarn Stroustrup | | B.S. is the creator of C++ 
| :     | colon        | | The name is: Gpit
| ;     | semicolon    | similarlly|
| i.e.  | in other words | similar to a ';'
| e.g.  | for example  | | E.g. = For example | 
| ...   | et cetera     | may/can be filled/finished  | | 
| '...' | single quote | my saying/idea |
| "..." | double quote | others saying/idea; quote| "I lost sort of half of my hair in the process of debugging..." --- B.S.
| \* | | wild card; fill some letters to complete the word  | "...f\* you" --- L.T.
| ...ror | will be reveal/released once ready | | the reason why only 'G' is capitalized and not 'pit'...ror   | 
|x/y    | divide line  | pick one (or more); or; alias | axiom/assumption/premise|
| x \@y  | at           | x is hosted at y | 3B1B @YouTube | 
| & | and | | | 
| b.c.  | because      |         |       | 
| Idk   | I don't know | ||
| s.t.  | such that / so that | 
| pi.0   | 0th pi | | pi.0 = This theory is perceptually simple and easy
| \,\,\,|ror|ror | |
Gpit| General perception-of-info theory\,\,\,
\*\*\*| \,\,\,
\*\*\*\*\*| \,\,\,
| link  |  click/go | | click here for 'p'; here for 'i'| 
\,\,\, | \,\,\, | \,\,\,| \,\,\,|

